<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:url"            content="http://moringa.adzumi.co.ke" />
    <meta property="og:type"           content="website" />
    <meta property="og:title"          content="Moringa Assignment Two" />
    <meta property="og:description"    content="Project required to qualify for the Moringa Core Program." />

    <title>Moringa Core Assignment Two</title>

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> 
    <script src="{{ URL::asset('public/js/jquery-1.11.1.min.js') }}"></script>

    <!-- Styles -->
    <link href="{{ URL::asset('public/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('public/css/bootstrap-override.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('public/css/font-awesome.min.css') }}" rel="stylesheet">


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77951077-3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-77951077-3');
    </script>
</head>




<body>
    <!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
    <nav id="menu-wrapper">
        <ul class="menu-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
            <li class="menu-brand">
                <a href="#top"></a>
            </li>
            <li>
                <a href="#">Contacts</a>
            </li>
            <li>
                <a href="#">Events</a>
            </li>
            <li>
                <a href="#">Menu</a>
            </li>
        </ul>
    </nav>

    <!-- Header -->
    <header id="top" class="header">
        <div class="text-vertical-center">
            <img src="{{ URL::asset('public/images/logoBig.png') }}">
            <h3>We Provide The Best Food<br> At The <strong>Right Price</strong></h3>
        </div>
    </header>

    <!-- Breakfast, lunch and dinner Section -->
    <section class="food-type">
        <div class="row">
            <div class="col-lg-3 text-center">
                <div class="row happy-hour" data-toggle="modal" data-target="#happy-hour">
                    <p>Happy Hour</p>
                </div>
                <div class="row party-time" data-toggle="modal" data-target="#party-time">
                    <p>Party Time</p>
                </div>
            </div>
            <div class="col-lg-3 text-center breakfast">
                <p>Breakfast</p>
            </div>
            <div class="col-lg-3 text-center lunch">
                <p>Lunch</p>
            </div>
            <div class="col-lg-3 text-center dinner">
                <p>Dinner</p>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row fast-food">
                <div class="col-lg-6 images">
                    <img src="{{ URL::asset('public/images/fastFood1.jpg') }}">
                    <img src="{{ URL::asset('public/images/fastFood2.jpg') }}">
                </div>
                <div class="col-lg-6 details">
                    <h1>Fast Food</h1>
                    <h3>You'll love our delicious food.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Sed quia non numquam eius modi tempora. </p>
                    <a href="#" class="btn btn-transaparent">VIEW GALLERY&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                </div>
            </div>
            <div class="row drinks">
                <div class="col-lg-6 details">
                    <h1>Drinks</h1>
                    <h3>We Also Produce Delicious Drinks.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Sed quia non numquam eius modi tempora. </p>
                    <a href="#" class="btn btn-transaparent">VIEW GALLERY&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                </div>
                <div class="col-lg-6 images">
                    <img class="pull-left" src="{{ URL::asset('public/images/drink1.jpg') }}">
                    <img src="{{ URL::asset('public/images/drink2.jpg') }}">
                </div>
            </div>
        </div>
    </section>


    <section class="dishes">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#fast_food" data-toggle="tab"><strong>Fast Food</strong></a></li>
            <li><a href="#drinks" data-toggle="tab"><strong>Drinks</strong></a></li>
            <li><a href="#chicken" data-toggle="tab"><strong>Chicken</strong></a></li>
            <li><a href="#rice" data-toggle="tab"><strong>Rice</strong></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="fast_food">
                
                <div id="featured-customers-carousel" class="carousel slide carousel-b" data-ride="">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#featured-customers-carousel" data-slide-to="0" class="active"></li>
                        <li class="" data-target="#featured-customers-carousel" data-slide-to="1"></li>
                        <li class="" data-target="#featured-customers-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">            
                        <div class="item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/chickenBurger.jpg') }}">
                                        <p>Chicken Burger</p>
                                    </div>
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/beefBurger.jpg') }}">
                                        <p>Beef Burger</p>
                                    </div>
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/chickenPizza.jpg') }}">
                                        <p>Chicken Pizza</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/chickenBurger.jpg') }}">
                                        <p>Chicken Burger</p>
                                    </div>
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/beefBurger.jpg') }}">
                                        <p>Beef Burger</p>
                                    </div>
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/chickenPizza.jpg') }}">
                                        <p>Chicken Pizza</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/chickenBurger.jpg') }}">
                                        <p>Chicken Burger</p>
                                    </div>
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/beefBurger.jpg') }}">
                                        <p>Beef Burger</p>
                                    </div>
                                    <div class="col-lg-4">
                                        <img src="{{ URL::asset('public/images/fastfood/chickenPizza.jpg') }}">
                                        <p>Chicken Pizza</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <a style="display: block;" class="left carousel-control hidden-xs" href="#featured-customers-carousel" role="button" data-slide="prev" data-wrap="false">
                        <span id="prevArrow" class="arrow-dark" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a style="display: block;" class="right carousel-control hidden-xs" href="#featured-customers-carousel" role="button" data-slide="next" data-wrap="false">
                        <span id="nextArrow" class="arrow-dark" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div><!-- /.carousel -->
            </div>
            <div class="tab-pane" id="drinks">
            </div>
            <div class="tab-pane" id="chicken">
            </div>
            <div class="tab-pane" id="rice">
            </div>
        </div>
    </section>


    <section class="customers">
        <div class="container">
            <div class="row content">
                <div class="col-lg-8 customer-details">
                    <h3>Our Happy Customers</h3>
                    <p><strong>George Richard</strong></p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                </div>
                <div class="col-lg-4 about-menu">
                    <h3>About Menu</h3>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam.</p>
                </div>
            </div>
        </div>
    </section>


    <section class="subscribe">
        <div class="container">
            <p>Get our daily food items delivered to your email.</p>
            <form role="form" method="post" action="{{ url('subscribe') }}">
                {{ csrf_field() }}
                <input type="email" name="email" autocomplete="off" placeholder="Your email">
                <button class="btn btn-warning" type="submit">SUBSCRIBE</button>
            </form>
        </div>
    </section>

    <section class="footer">
        <div class="logo">
            <img src="{{ URL::asset('public/images/footerlogos/logo_white2.png') }}">
        </div>   
        <p>That's What We Are</p>  
        <div class="social">
            <ul class="icon_list">
                <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-dribbble"></i></a></li>
            </ul>
        </div>  
    </section>

    <footer>        
        <p class="copyright">2016 Menu Restaurant</p> 
    </footer>


<!-- Modals -->
<div id="happy-hour" class="modal fade bs-example-modal-panel in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="panel panel-dark panel-alt">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a data-dismiss="modal" aria-hidden="true">×</a>
                    </div><!-- panel-btns -->
                    <h4>Happy Hour</h4>
                </div>
                <div class="panel-body">
                    <div class="mt10">
                        <p class="note">After a long day at work, what comes to mind? How about a meet up with your pals for a little treat of the finest food and drink at any of your favorite happy hour spots? Happy hour after all is not just about getting a discount on drinks to calm your mind. It can also be about reconnecting with your long lost friends or acquaintances throughout the week and not necessarily on the weekends alone. It’s about ending the day, even if it didn’t start in a good way, in a fun-filled setting of refreshments, bitings and drinks.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="party-time" class="modal fade bs-example-modal-panel in" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="panel panel-dark panel-alt">
                <div class="panel-heading">
                    <div class="panel-btns">
                        <a data-dismiss="modal" aria-hidden="true">×</a>
                    </div><!-- panel-btns -->
                    <h4>Party Time</h4>
                </div>
                <div class="panel-body">
                    <div class="mt10">
                    <p class="note">Party Time  provides tents, linen - everything for weddings, picnics, tradeshows, or any special event. In all season of the year, for all sizes of events, Party Time will travel to provide quality rentals and set up services second to none in Nairobi, Mombasa, and Kisumu.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>
<script>
    
// Opens the menu
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#menu-wrapper").toggleClass("active");
});

// Closes the menu
$("#menu-close").click(function(e) {
    e.preventDefault();
    $("#menu-wrapper").toggleClass("active");
});

$("div.breakfast,div.lunch,div.dinner").hover(function(){
    $(this).addClass("opacity");
}, 
function(){
    $(this).removeClass("opacity");
});

</script>
</body>
</html>
