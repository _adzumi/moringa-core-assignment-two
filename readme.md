# Moringa Core Assignment Two

## Getting Started

On this project I was tasked with creating a website given a sample image to start from. The sites should be similar and I shouldn't deviate from the layout design.
I developed this application using the [Laravel Framework](https://laravel.com/docs) together with jQuery to compliment some of the features and functionalities required for the application to function.

### Requirements

Before trying to deploy a Laravel application on a shared hosting, you need to make sure that the hosting services provide a fit requirement to Laravel. Basically, following items are required for Laravel 5.4:

- PHP >= 5.6.0
- OpenSSL PHP Extension
- PDO PHP Extension

Besides PHP and those required extensions, you might need some utilities to make deployment much easier.
- [Git](https://git-scm.com/)
- [Composer](https://getcomposer.org/)

### Installing

- Clone this repository to a location on your file system.
- `cd` into the directory, run `php artisan serve` to start the server.
- Navigate to `localhost:8000` in your browser.

### Objectives

- Use Bootstrap
- Don't change site layout
- Create a Hover effect** for the **breakfast, lunch and dinner ** sections such that on hover they are covered by a layer of white and their fonts are turned to black from white.
- Create menu when clicked should open a full screen menu that has the following menu items:
       a. contacts
       b. events
       c. menu
  They should all be a hyperlink to '#' (does not redirect externally)
- The **full screen menu (above)** should be colour:** #F15A29 ** and **the text of the menu items black in colour**
- **Integrate Google Analytics** to track page visits to the website.
- Make the subscribe button to work using the mail chimp API.
- For the **Happy Hour and Party time,** create a modal that explains further what services Menu restaurant offers for both Party time and Happy Hour.



